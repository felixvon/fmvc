package top.felixvon.fmvc;

import top.felixvon.fmvc.aop.Aop;
import top.felixvon.fmvc.util.ClassUtil;

/**
 * 初始化框架时加载相应的类
 *
 * @author: felix
 * @data: 12/24/18
 * @time: 8:57 PM
 * @email: felix@felixvon.cn
 * @since: 1.0
 */
public final class Loader {

    /**
     * 舒适化时加载指定的类
     */
    public static void init() {
        Class<?>[] classList = {
                Aop.class,
        };

        for(Class<?> cla : classList) {
            ClassUtil.loadClass(cla.getName());
        }

    }
}
