package top.felixvon.fmvc.util;

import java.net.URL;

/**
 * 对类进行相关操作的工具
 *
 * @author: felix
 * @data: 12/24/18
 * @time: 9:07 PM
 * @email: felix@felixvon.cn
 * @since: 1.0
 */
public final class ClassUtil {

    /**
     * 获取类加载器
     *
     * @return 当前线程的类加载器
     */
    public static ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    /**
     * 获取类的路径(调用此方法时建议验证返回值)
     *
     * @return 类的路径
     */
    public static String getClassPath() {
        String classPath = "";
        URL resource = getClassLoader().getResource("");
        if(resource != null) {
            classPath = resource.getPath();
        }
        return classPath;
    }

    /**
     * 根据提供类名加载相应的类(自动初始化类)
     *
     * @param className 类名
     * @return 加载完成的类
     */
    public static Class<?> loadClass(String className) {
        return loadClass(className, true);
    }

    /**
     * 根据提供的类名加载相应的类,第二个参数: isInit 指定是否初始化类
     *
     * @param className 类名
     * @param isInit 是否初始化
     * @return 加载完成的类
     */
    public static Class<?> loadClass(String className, boolean isInit) {
        Class<?> cla;
        try {
            cla = Class.forName(className, isInit, getClassLoader());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return cla;
    }
}
